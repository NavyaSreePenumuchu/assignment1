package com.service;


import java.util.List;

import com.model.Medicine;
import com.model.Prescription;

public interface PrescriptionServiceIntf {
	
	
	void savePrescription(Prescription prescription);

	List<Prescription> prescriptionDetails();
	public int deletePrescriptionById(int presid);
	public List<Prescription> viewPrescriptionById(int presid);
	Prescription getPrescriptionById(int presid);

	void updatePrescription(Prescription prescription);
	List<Medicine> medicineDetails(int id);

}
