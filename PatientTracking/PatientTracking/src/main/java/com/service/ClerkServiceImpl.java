package com.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ClerkDaoIntf;
import com.model.Clerk;
@Service
@Transactional
public class ClerkServiceImpl implements ClerkServiceIntf{
	@Autowired
	ClerkDaoIntf dao;
	public void saveClerk(Clerk clerk) {
		dao.saveClerk(clerk);
		
	}
	
	public List<Clerk> clerkDetails() {
		 List<Clerk>li= dao.clerkDetails();
		 return li;
		
	}

	
	public Clerk getClerkById(int id) {
				return dao.getClerkById(id);
	}

	
	public void updateClerk(Clerk clerk) {
		dao.updateClerk(clerk);
		
	}

	
	public int deleteClerkById(int id) {
		
		return dao.deleteClerkById(id);
	}

	
	public List<Clerk> viewClerkById(int clerkid) {
		List<Clerk> li=dao.viewClerkById(clerkid);
		return li;
	}

}
