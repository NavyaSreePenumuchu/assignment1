package com.service;

import java.util.List;

import com.model.Medicine;
import com.model.Patient;


public interface PatientServiceIntf {


	void savePatient(Patient patient);
	List<Patient> patientDetails();
	int deletePatientById(int patientid);
	List<Patient> viewPatientById(int patientid);
	Patient getPatientById(int patientid);
	void updatePatient(Patient patient);
	List<Patient> getPatientDetails(int id);
	public List<Patient> getBillDetails(int id);

	public List<Medicine> getMedicineDetails(int id);
}
