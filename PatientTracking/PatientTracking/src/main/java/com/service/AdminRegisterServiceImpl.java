package com.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.AdminRegisterDaoIntf;
import com.model.AdminRegister;
@Service
@Transactional
public class AdminRegisterServiceImpl implements AdminRegisterServiceIntf{
	@Autowired
	AdminRegisterDaoIntf dao;
	
	public void saveAdminRegister(AdminRegister register) {
				dao.saveAdminRegister(register);
	}

	
	public boolean checkAdmin(int adminid, String password) {
		
		 return dao.checkAdmin(adminid,password);
	}


	
	
	
	public void forgotPassword(int adminId, String password) {
		 dao.forgotPassword(adminId,password);
		
	}

}
