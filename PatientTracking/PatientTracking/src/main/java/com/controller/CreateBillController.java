package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Medicine;
import com.model.Patient;
import com.service.PatientServiceIntf;
import com.service.PrescriptionServiceIntf;

@Controller
public class CreateBillController {
	 @Autowired
	    PrescriptionServiceIntf service;
	 @Autowired
	    PatientServiceIntf pservice;
@RequestMapping("/getMedicine/{id}")
public ModelAndView getMedicineDetails(@PathVariable int id) {
	List<Medicine> li=service.medicineDetails(id);
	 ModelAndView model = new ModelAndView("billdetails");
    model.addObject("medicinedetails", li);
    return model;

	
}
@RequestMapping("/getPatientDetails/{id}")
public ModelAndView patientDetails(@PathVariable int id) {
	List<Patient> li=pservice.getPatientDetails(id);
	 ModelAndView model = new ModelAndView("createbilldetails");
     model.addObject("patientdetails", li);
     return model;
}
@RequestMapping("/generateBill/{id}")
public ModelAndView generateBill(@PathVariable int id) {
	//List<Patient>li=pservice.getBillDetails(id);
	List<Medicine> li=pservice.getMedicineDetails(id);
	return new ModelAndView("generatebillform","bill",li);
}
}
