package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.model.Medicine;
import com.model.Patient;
import com.service.MedicineServiceIntf;


	@Controller
	public class MedicineController {
	     @Autowired
	        MedicineServiceIntf service;
	    @RequestMapping("/medicine")
	    public ModelAndView registerPage(@ModelAttribute("medicine") Medicine medicine)
	    {
	        return new ModelAndView("medicine");
	    }
	    @RequestMapping("/saveMedicine")
	    public ModelAndView saveRegister(@Validated @ModelAttribute("medicine") Medicine medicine,BindingResult result) {
	        if(result.hasErrors())
	        {
	            return new ModelAndView("medicine");
	        }
	        else
	        {
	            service.saveMedicine(medicine);
	           // return new ModelAndView("redirect:/fetchUsers");
	             //return new ModelAndView("success");
	            return new ModelAndView("redirect:/medicinedetails");
	        }
	    }
		@RequestMapping("/medicinedetails")
		public ModelAndView medicineDetails() {
			List<Medicine> li=service.medicineDetails();
			 ModelAndView model = new ModelAndView("medicinedetails");
		     model.addObject("medicinedetails", li);
		     return model;
		}
		@RequestMapping("/deleteMedicine/{medicineid}")
		public ModelAndView deleteMedicine(@PathVariable("medicineid") int medicineid,Model model) {
		int result=service.deleteMedicineById(medicineid);
		    model.addAttribute("result","deleted successfully");
		    return new ModelAndView("redirect:/medicinedetails");
	
	}
		@RequestMapping("/searchMedicineById")
	    public ModelAndView searchMedicineById() {
			return new ModelAndView("searchmedicineid");
		}
		 @RequestMapping("/searchMedicineId")
		    public ModelAndView viewMedicineById(@RequestParam("medicineid") int medicineid) {
			 List<Medicine> medicine=service.viewMedicineById(medicineid);
		     ModelAndView model = new ModelAndView("viewmedicineid");
		     model.addObject("view", medicine);
		     return model;
		 }
		 @RequestMapping(value="/editMedicine/{medicineid}")
			public ModelAndView editMedicine(@PathVariable int medicineid,Model model) {
				Medicine medicine=service.getMedicineById(medicineid);
				model.addAttribute("command", medicine);
				return new ModelAndView("medicineeditform");
			}
			@RequestMapping(value="/editSaveMedicine")
			public ModelAndView editMedicineSave(Medicine medicine) {
				service.updateMedicine(medicine);
				return new ModelAndView("redirect:/medicinedetails");

	}
}