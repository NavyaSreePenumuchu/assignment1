package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.model.Patient;
import com.model.Prescription;
import com.service.PrescriptionServiceIntf;

	@Controller
	public class PrescriptionController {
	
	    @Autowired
	    PrescriptionServiceIntf service;
	@RequestMapping("/prescription")
	public ModelAndView registerPage(@ModelAttribute("prescription") Prescription prescription) {
	    
	    return new ModelAndView("prescription");
	    
	}
	@RequestMapping("/savePrescription")
	public ModelAndView saveRegister(@Validated @ModelAttribute("prescription") Prescription prescription,BindingResult result) {
	   if(result.hasErrors()) {
	        return new ModelAndView("prescription");
	    }
	    else {
	    	
	        service.savePrescription(prescription);
	   //return new ModelAndView("redirect:/fetchUsers");
	        //return new ModelAndView("success");
	        return new ModelAndView("redirect:/prescriptiondetails");
	    }  
	}

	@RequestMapping("/prescriptiondetails")
	public ModelAndView prescriptionDetails() {
		List<Prescription> li=service.prescriptionDetails();
		 ModelAndView model = new ModelAndView("prescriptiondetails");
	     model.addObject("prescriptiondetails", li);
	     return model;
	}
	@RequestMapping("/deletePrescription/{presid}")
	public ModelAndView deletePatient(@PathVariable("presid") int presid,Model model) {
	int result=service.deletePrescriptionById(presid);
	    model.addAttribute("result","deleted successfully");
	    return new ModelAndView("redirect:/prescriptiondetails");
	}
	/*@RequestMapping("/searchPrescriptionById")
    public ModelAndView searchPrescriptionById() {
		return new ModelAndView("searchprescriptiontid");
	}*/
	@RequestMapping("/searchPrescriptionById")
    public ModelAndView searchPrescriptionById() {
		return new ModelAndView("searchprescriptionid");
	}
	 @RequestMapping("/searchPrescriptionId")
	    public ModelAndView viewPrescriptionById(@RequestParam("presid") int presid) {
		 List<Prescription> prescription=service.viewPrescriptionById(presid);
	     ModelAndView model = new ModelAndView("viewprescriptionid");
	     model.addObject("view", prescription);
	     return model;
	 }
	 @RequestMapping(value="/editPrescription/{presid}")
		public ModelAndView editPrescription(@PathVariable int presid,Model model) {
			Prescription prescription=service.getPrescriptionById(presid);
			model.addAttribute("command", prescription);
			return new ModelAndView("prescriptioneditform");
		}
		@RequestMapping("/editSavePrescription")
		public ModelAndView editPrescriptionSave(Prescription prescription) {
			service.updatePrescription(prescription);
			return new ModelAndView("redirect:/prescriptiondetails");

}
		@RequestMapping(value="/createBill")
        public ModelAndView createBill(){
			List<Prescription> li=service.prescriptionDetails();
			 ModelAndView model = new ModelAndView("createbill");
		     model.addObject("prescriptiondetails", li);
		     return model;
		
        

 

        }
	}		

