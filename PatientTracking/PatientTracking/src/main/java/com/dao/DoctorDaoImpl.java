package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Clerk;
import com.model.Doctor;
@Repository
public class DoctorDaoImpl implements DoctorDaoIntf{
	@Autowired
	SessionFactory sessionFactory;
	public void saveDoctor(Doctor doctor) {
		sessionFactory.openSession().save(doctor);	
	}


	
	
	
	public List<Doctor> doctorDetails() {
		List<Doctor> li=(List<Doctor>) sessionFactory.openSession().createQuery("from Doctor").list();
		return li;
	
	}





	
	




	
	public void saveEditDoctor(Doctor doctor) {
		sessionFactory.openSession().saveOrUpdate(doctor);
	}





	
	public int deleteDoctorById(int id) {
		int result=0;
		Doctor doctor=(Doctor) sessionFactory.openSession().load(Doctor.class, id);	
		if(doctor!=null) {
			Query query=sessionFactory.openSession().createQuery("delete from Doctor d where d.id=:id");
			query.setParameter("id", id);
			 result=query.executeUpdate();
			
		}
		return result;
	}





	
	public List<Doctor> viewDoctorById(int doctorid) {
		Query query= (Query) sessionFactory.openSession().createQuery("from Doctor d where d.id=?");
		query.setParameter(0,doctorid);
		List<Doctor> li=query.list();
			return li;
		}





	
	public Doctor getDoctorById(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from doctor where Doctor_id=:id").addEntity(Doctor.class);
		query.setParameter("id",id);
		Doctor doctor= (Doctor) query.uniqueResult();
		return doctor;
		
	}





	
	public void updateDoctor(Doctor doctor) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update doctor set Doctor_FirstName=:name,Doctor_Age=:age,Doctor_ContactNumber=:number,Doctor_Domain=:domain,Doctor_Gender=:gender,Doctor_LastName=:lname where Doctor_id=:id").addEntity(Doctor.class);
		query.setParameter("name",doctor.getFirstname());
		query.setParameter("age", doctor.getAge());
		query.setParameter("number", doctor.getContactnumber());
		query.setParameter("domain",doctor.getDomain());
		query.setParameter("gender",doctor.getGender());
		query.setParameter("lname",doctor.getLastname());
		query.setParameter("id",doctor.getId());
		query.executeUpdate();	
		
	}	


	}
