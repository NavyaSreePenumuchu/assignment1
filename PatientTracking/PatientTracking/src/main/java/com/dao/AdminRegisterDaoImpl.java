package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.AdminRegister;
@Repository
public class AdminRegisterDaoImpl implements AdminRegisterDaoIntf {
	@Autowired
	SessionFactory sessionFactory;
	public void saveAdminRegister(AdminRegister register) {
		//System.out.println("bye");
		sessionFactory.openSession().save(register);
		
	}
	
	public boolean checkAdmin(int adminid, String password) {
		 boolean userFound = false;
	        String sql =" from AdminRegister as o where o.adminid=? and o.password=?";
	        //String sql="select * from adminregister where adminid=? and password=?";
	        Query query=sessionFactory.openSession().createQuery(sql);
	        
	        query.setParameter(0,adminid);
	        query.setParameter(1,password);
	        List list = query.list();

	 

	        if ((list != null) && (list.size() > 0)) {
	            userFound= true;
	        }

	 

	    
	        return userFound; 
	        
	    }

	
	
	
	public void forgotPassword(int adminId, String password) {
		
		Query query=sessionFactory.openSession().createQuery("update AdminRegister a set a.password=:password where a.adminid=:id");
		query.setParameter("id", adminId);
		query.setParameter("password", password);
		query.executeUpdate();
		
	}

	
	

	 
		
	}


