<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style>
.error{
color:red;
}
.view{
bordered-collapse:collapse;
background-color:cyan;
}
[type="submit"],[type="reset"]{
background-color:blue;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body><br><br>
<div align="center">
<h3 style="color:tomato">Registration Form</h3>
<table border="1"  class="view"  >
<form:form action="saveRegister" modelAttribute="adminregister" method="post">
<tr>
<th>First Name</th><td><form:input path="firstname"/><form:errors cssClass="error" path="firstname"></form:errors></td></tr>
<tr><th>Last Name</th><td><form:input path="lastname"/><form:errors cssClass="error" path="lastname"></form:errors></td></tr>
<tr><th>Age</th><td><input type="text" name="age"/><form:errors cssClass="error" path="age"></form:errors></td></tr>
<tr><th>Password</th><td><form:password path="password"/><form:errors cssClass="error" path="password"></form:errors></td></tr>
<tr><th>gender</th><td><form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female<form:errors cssClass="error" path="gender"></form:errors></td></tr>
<tr><th>Contact Number</th><td><form:input path="contactnumber"/><form:errors cssClass="error" path="contactnumber"></form:errors></td>
<tr><td></td><th>

<input type="submit" value="Register" class="btn btn-primary">&nbsp;&nbsp;
<input type="reset" value="Cancel" class="btn btn-danger"></th>
</form:form>
</table>
</div>
</body>
</html>