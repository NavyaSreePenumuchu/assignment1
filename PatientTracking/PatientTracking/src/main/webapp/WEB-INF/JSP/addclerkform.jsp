<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.error{
color:red;
}
.add{
border-collapse:collapse;
background-color:#ffffff00;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">AddClerk</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li> 
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>   
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>

<br>
<div align="right">Search Clerk By Id <a href="searchClerkById">searchClerkById</a></div>
<br>
<div align="center">
<h3 style="color:tomato">Add Clerk</h3>
<table border="1" class="add" >
<form:form action="saveClerk" modelAttribute="clerk" method="post">
<tr>
<th>First Name</th><td><form:input path="firstname"/><form:errors cssClass="error" path="firstname"></form:errors></td></tr>
<tr><th>Last Name</th><td><form:input path="lastname"/></td></tr>
<tr><th>Age</th><td><input type="text" name="age"/><form:errors cssClass="error" path="age"></form:errors></td></tr>
<tr><th>gender</th><td><input type="radio" name="gender" value="Male">Male
<input type="radio" name="gender" value="Female">Female</td></tr>
<tr><th>Contact Number</th><td><form:input path="contactnumber"/><form:errors cssClass="error" path="contactnumber"></form:errors></td></tr>
<tr><th></th><td><input type="submit" value="Add Clerk" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
</form:form>
</table>
</div>
</body>
</html>