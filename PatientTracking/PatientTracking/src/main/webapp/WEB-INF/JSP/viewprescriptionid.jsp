<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.view
{border-collapse:collapse;
background-color:cyan;
}
th{
background-color:tomato;
}


</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">ViewPrescriptionById</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>


 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
       
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>
      <br><br> 
       </li> 
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>   
    </ul>
  </div>  
</nav><br><br>
<table border="1" class="table" class="view">
<tr><th>PrescriptionId</th><th>Name</th><th>age</th><th>weight</th><th>gender</th><th>Medicinename</th>
<th>Take</th><th>Drug Form</th><th>Route</th><th>Duration(days)</th></tr>
<c:forEach var="prescription" items="${view}">
<tr>
<td>${prescription.presid}</td>
<td>${prescription.name}</td>
<td>${prescription.age}</td>
<td>${prescription.weight}</td>
<td>${prescription.gender}</td>
<td>${prescription.medicinename}</td>
<td>${prescription.take}</td> 
<td>${prescription.drugform}</td>
<td>${prescription.route}</td>
<td>${prescription.duration}</td>βββ
</tr>
</c:forEach>
</table>
</body>
</html>

