<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<script>
function validate(){
var adminId=document.form.adminid.value;
var password=document.form.password.value;
var cpassword=document.form.cpassword.value;
if(password!=cpassword){
	alert("password and confirm password should be same")
}
}
</script>
<style type="text/css">
.view{
background-color:cyan;
border-collapse:collapse;
}
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body><br>
<div align="center">
<h3 style="color:tomato">Change Password</h3>
<form action="savepassword" name="form" onsubmit="return validate()" method="post">
<table border="1" class="view">
<tr>
<th>Admin Id</th>
<td><Input type="text" name="adminid" placeholder="enter adminid"></td>
</tr>
<tr>
<th>Password </th>
<td><Input type="password" name="password" placeholder="enter password"></td>
</tr>
 <tr>
<th> Confirm Password </th>
<td><Input type="password" name="cpassword" placeholder="enter confirm password"></td>
</tr>
<tr>
<th></th>
<td><Input type="submit" value="Save Password" class="btn btn-primary">
<Input type="reset" name="cancel" value="Cancel" class="btn btn-danger"></td>
</tr>
</table>
</form>
</div>
</body>
</html>