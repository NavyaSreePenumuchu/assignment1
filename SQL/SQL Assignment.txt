Create Table 

1.Write a SQL statement to create a simple table countries including columns country_id,country_name and region_id 

mysql> create table countries(country_id int,country_name varchar(20),region_id varchar(20)); 
Query OK, 0 rows affected (1.63 sec) 

mysql> insert into countries values(234,"India",345); 
Query OK, 1 row affected (0.16 sec) 

mysql> insert into countries values(235,"Sri Lanka",346); 
Query OK, 1 row affected (0.13 sec) 

mysql> insert into countries values(236,"Turkey",678); 
Query OK, 1 row affected (0.09 sec) 

mysql> insert into countries values(237,"Pakistan",978); 
Query OK, 1 row affected (0.14 sec) 

mysql> desc countries; 
+--------------+-------------+------+-----+---------+-------+ 

| Field        | Type        | Null | Key | Default | Extra | 

+--------------+-------------+------+-----+---------+-------+ 

| country_id   | int         | YES  |     | NULL    |       | 

| country_name | varchar(20) | YES  |     | NULL    |       | 

| region_id    | int         | YES  |     | NULL    |       | 

+--------------+-------------+------+-----+---------+-------+ 

3 rows in set (0.23 sec) 

 
mysql> select * from countries; 
+------------+--------------+-----------+ 

| country_id | country_name | region_id | 

+------------+--------------+-----------+ 

|        234 | India        |       345 | 

|        235 | Sri Lanka    |       346 | 

|        236 | Turkey       |       678 | 

|        237 | Pakistan     |       978 | 

+------------+--------------+-----------+ 

4 rows in set (0.04 sec) 

 
2.Write a SQL statement to create a table countries set a constraint NULL 

mysql> create table countries(country_id int null,country_name varchar(20),region_id int); 
Query OK, 0 rows affected (0.40 sec) 
mysql> desc countries; 
+--------------+-------------+------+-----+---------+-------+ 

| Field        | Type        | Null | Key | Default | Extra | 

+--------------+-------------+------+-----+---------+-------+ 

| country_id   | int         | YES  |     | NULL    |       | 

| country_name | varchar(20) | YES  |     | NULL    |       | 

| region_id    | int         | YES  |     | NULL    |       | 

+--------------+-------------+------+-----+---------+-------+ 

3 rows in set (0.07 sec) 

mysql> insert into countries values(234,"India",345); 

Query OK, 1 row affected (0.08 sec) 
mysql> insert into countries values(235,"Sri Lanka",346); 

Query OK, 1 row affected (0.09 sec) 

mysql> insert into countries values(236,"Turkey",678); 
Query OK, 1 row affected (0.07 sec) 

mysql> insert into countries values(237,"Pakistan",978); 
Query OK, 1 row affected (0.12 sec) 

mysql> insert into countries(country_name,region_id) values("Kizitiskan",378); 
Query OK, 1 row affected (0.07 sec)
mysql> select * from countries; 
+------------+--------------+-----------+ 

| country_id | country_name | region_id | 

+------------+--------------+-----------+ 

|        234 | India        |       345 | 

|        235 | Sri Lanka    |       346 | 

|        236 | Turkey       |       678 | 

|        237 | Pakistan     |       978 | 

|       NULL | Kizitiskan   |       378 | 

+------------+--------------+-----------+ 

5 rows in set (0.00 sec) 

3.create table 
3.create table locations including columns. 

LOCATION_ID          
decimal(4,0)  
STREET_ADDRESS   
varchar(40) 
POSTAL_CODE        
varchar(12) 
CITY    
varchar(30)  
STATE_PROVINCE  
varchar(25)  
COUNTRY_ID          
varchar(2) 
mysql> create table locations 

    -> ( 

    -> LOCATION_ID    decimal(4,0) , 

    -> STREET_ADDRESS  varchar(40), 

    -> POSTAL_CODE  varchar(12), 

    -> CITY   varchar(30), 

    -> STATE_PROVINCE varchar(25), 

    -> COUNTRY_ID   varchar(2) ); 

Query OK, 0 rows affected (1.82 sec) 
mysql> desc locations; 
+----------------+--------------+------+-----+---------+-------+ 

| Field          | Type         | Null | Key | Default | Extra | 

+----------------+--------------+------+-----+---------+-------+ 

| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       | 

| STREET_ADDRESS | varchar(40)  | YES  |     | NULL    |       | 

| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       | 

| CITY           | varchar(30)  | YES  |     | NULL    |       | 

| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       | 

| COUNTRY_ID     | varchar(2)   | YES  |     | NULL    |       | 

+----------------+--------------+------+-----+---------+-------+ 

6 rows in set (0.09 sec) 

 

4. 

 

Alter Table 

1.Write a SQL statement to rename the table countries to country_new. 

mysql> alter table countries rename to country_new; 
mysql> desc countries; 
ERROR 1146 (42S02): Table 'amazondb.countries' doesn't exist 
mysql> desc country_new; 
+--------------+-------------+------+-----+---------+-------+ 

| Field        | Type        | Null | Key | Default | Extra | 

+--------------+-------------+------+-----+---------+-------+ 

| country_id   | int         | YES  |     | NULL    |       | 

| country_name | varchar(20) | YES  |     | NULL    |       | 

| region_id    | int         | YES  |     | NULL    |       | 

+--------------+-------------+------+-----+---------+-------+ 

3 rows in set (0.24 sec) 

 
2.Write a SQL statement to add a columns ID as the first column of the table locations 

mysql> alter table country_new add columnsID int first; 
mysql> desc country_new; 
+--------------+-------------+------+-----+---------+-------+ 

| Field        | Type        | Null | Key | Default | Extra | 

+--------------+-------------+------+-----+---------+-------+ 

| columnsID    | int         | YES  |     | NULL    |       | 

| country_id   | int         | YES  |     | NULL    |       | 

| country_name | varchar(20) | YES  |     | NULL    |       | 

| region_id    | int         | YES  |     | NULL    |       | 

+--------------+-------------+------+-----+---------+-------+ 

4 rows in set (0.14 sec) 


3.Write a SQL statement to add a column region_id after state_province to the table locations 

mysql> alter table locations add region_id int after state_province; 
Query OK, 0 rows affected (2.39 sec) 
Records: 0  Duplicates: 0  Warnings: 0
mysql> desc locations; 
+----------------+--------------+------+-----+---------+-------+ 

| Field          | Type         | Null | Key | Default | Extra | 

+----------------+--------------+------+-----+---------+-------+ 

| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       | 

| STREET_ADDRESS | varchar(40)  | YES  |     | NULL    |       | 

| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       | 

| CITY           | varchar(30)  | YES  |     | NULL    |       | 

| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       | 

| region_id      | int          | YES  |     | NULL    |       | 

| COUNTRY_ID     | varchar(2)   | YES  |     | NULL    |       | 

+----------------+--------------+------+-----+---------+-------+ 

7 rows in set (0.00 sec) 


4.Write a SQL statement change the data type of the column country_id to integer in the table locations 

mysql> alter table locations modify location_id int; 
Query OK, 0 rows affected (1.79 sec) 
Records: 0  Duplicates: 0  Warnings: 0 
mysql> desc locations; 
+----------------+-------------+------+-----+---------+-------+ 

| Field          | Type        | Null | Key | Default | Extra | 

+----------------+-------------+------+-----+---------+-------+ 

| location_id    | int         | YES  |     | NULL    |       | 

| STREET_ADDRESS | varchar(40) | YES  |     | NULL    |       | 

| POSTAL_CODE    | varchar(12) | YES  |     | NULL    |       | 

| CITY           | varchar(30) | YES  |     | NULL    |       | 

| STATE_PROVINCE | varchar(25) | YES  |     | NULL    |       | 

| region_id      | int         | YES  |     | NULL    |       | 

| COUNTRY_ID     | varchar(2)  | YES  |     | NULL    |       | 

+----------------+-------------+------+-----+---------+-------+ 

7 rows in set (0.00 sec) 

  

Insert Table 

1.Write a SQL statement to insert 3 rows by a single insert statement 

mysql> insert into country_new values(123,345,"India",567),(124,566,"Pakistan",987),(456,234,"SriLanka",675); 
Query OK, 3 rows affected (0.13 sec) 
Records: 3  Duplicates: 0  Warnings: 0 

 
2.Write a SQL statement to insert rows into the table countries in which the value of country_id column will be unique and auto incremented 



3.Write a SQL statement to insert rows only for country_id and country_name. 

mysql> insert into countries(country_id,country_name) values(563,"Germany"),(456,"Australia"); 
Query OK, 2 rows affected (0.12 sec) 
Records: 2  Duplicates: 0  Warnings: 0 
mysql> select * from countries; 
+-----------+------------+--------------+-----------+ 

| columnsID | country_id | country_name | region_id | 

+-----------+------------+--------------+-----------+ 

|      NULL |        234 | India        |       345 | 

|      NULL |        235 | Sri Lanka    |       346 | 

|      NULL |        236 | Turkey       |       678 | 

|      NULL |        237 | Pakistan     |       978 | 

|      NULL |       NULL | Kizitiskan   |       378 | 

|       123 |        345 | India        |       567 | 

|       124 |        566 | Pakistan     |       987 | 

|       456 |        234 | SriLanka     |       675 | 

|      NULL |        563 | Germany      |      NULL | 

|      NULL |        456 | Australia    |      NULL | 

+-----------+------------+--------------+-----------+ 

10 rows in set (0.00 sec) 


Update Table 

Employees Table 

EMPLOYEE_ID 
FIRST_NAME 
EMAIL 
SALARY 
COMMISSION_PCT 
DEPARTMENT_ID 

100 
Steven 
SKING 
24000.00 
0.00 
90 
101 
Neena 
NKOCHHAR 
1700.00 
0.00 
90 
102 
Lex 
LDEHAAN 
17000.00 
0.00 
90 
103 
Alexander 
AHUNOLD 
9000.00 
0.00 
60 
104 
Bruce 
BERNST 
6000.00 
0.00 
60 
105 
David 
DAUSTIN 
4800.00 
0.00 
60 
106 
Valli 
VPATABAL 
4200.00 
0.00 
60 
107 
Diana 
DLORENTZ 
12008.00 
0.00 
110 
205 
Shelley 
SHIGGINS 
8300.00 
0.00 
110 
206 
William 
WGIETZ 
8300.00 
0.00 
110 


1.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for all employees 

mysql> update employess set email=null ,commission=0.1; 
Query OK, 0 rows affected (0.00 sec) 
Rows matched: 10  Changed: 0  Warnings: 0 
mysql> select * from employess; 
+-------------+------------+-------+----------+------------+---------------+ 

| employee_id | first_name | email | salary   | commission | department_id | 

+-------------+------------+-------+----------+------------+---------------+ 

|         100 | Steven     | NULL  | 24000.00 |       0.10 |            90 | 

|         101 | Neena      | NULL  |  1700.00 |       0.10 |            90 | 

|         102 | Lex        | NULL  | 17000.00 |       0.10 |            90 | 

|         103 | Alexander  | NULL  |  9000.00 |       0.10 |            60 | 

|         104 | Bruce      | NULL  |  6000.00 |       0.10 |            60 | 

|         105 | David      | NULL  |  4800.00 |       0.10 |            60 | 

|         106 | Valli      | NULL  |  4200.00 |       0.10 |            60 | 

|         107 | Diana      | NULL  | 12008.00 |       0.10 |           110 | 

|         205 | Shelley    | NULL  |  8300.00 |       0.10 |           110 | 

|         206 | William    | NULL  |  8300.00 |       0.10 |           110 | 

+-------------+------------+-------+----------+------------+---------------+ 

10 rows in set (0.00 sec) 


2.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for those employees whose department_id is 110. 

mysql> update employess set email=null ,commission=0.10 where department_id=110; 
Query OK, 3 rows affected (0.10 sec) 
Rows matched: 3  Changed: 3  Warnings: 0 
mysql> select * from employess; 
+-------------+------------+----------+----------+------------+---------------+ 

| employee_id | first_name | email    | salary   | commission | department_id | 

+-------------+------------+----------+----------+------------+---------------+ 

|         100 | Steven     | SKING    | 24000.00 |       0.00 |            90 | 

|         101 | Neena      | NKOCHHAR |  1700.00 |       0.00 |            90 | 

|         102 | Lex        | LDEHAAN  | 17000.00 |       0.00 |            90 | 

|         103 | Alexander  | AHUNOLD  |  9000.00 |       0.00 |            60 | 

|         104 | Bruce      | BERNST   |  6000.00 |       0.00 |            60 | 

|         105 | David      | DAUSTIN  |  4800.00 |       0.00 |            60 | 

|         106 | Valli      | VPATABAL |  4200.00 |       0.00 |            60 | 

|         107 | Diana      | NULL     | 12008.00 |       0.10 |           110 | 

|         205 | Shelley    | NULL     |  8300.00 |       0.10 |           110 | 

|         206 | William    | NULL     |  8300.00 |       0.10 |           110 | 

+-------------+------------+----------+----------+------------+---------------+ 

10 rows in set (0.05 sec)
 

3.Write a SQL statement to change salary of employee to 8000 whose ID is 105, if the existing salary is less than 5000. 

mysql> update employess set salary=8000 where employee_id=105 and salary<5000; 

Query OK, 1 row affected (0.10 sec) 
Rows matched: 1  Changed: 1  Warnings: 0 
mysql> select * from employess; 
+-------------+------------+----------+----------+------------+---------------+ 

| employee_id | first_name | email    | salary   | commission | department_id | 

+-------------+------------+----------+----------+------------+---------------+ 

|         100 | Steven     | SKING    | 24000.00 |       0.00 |            90 | 

|         101 | Neena      | NKOCHHAR |  1700.00 |       0.00 |            90 | 

|         102 | Lex        | LDEHAAN  | 17000.00 |       0.00 |            90 | 

|         103 | Alexander  | AHUNOLD  |  9000.00 |       0.00 |            60 | 

|         104 | Bruce      | BERNST   |  6000.00 |       0.00 |            60 | 

|         105 | David      | DAUSTIN  |  8000.00 |       0.00 |            60 | 

|         106 | Valli      | VPATABAL |  4200.00 |       0.00 |            60 | 

|         107 | Diana      | NULL     | 12008.00 |       0.10 |           110 | 

|         205 | Shelley    | NULL     |  8300.00 |       0.10 |           110 | 

|         206 | William    | NULL     |  8300.00 |       0.10 |           110 | 

+-------------+------------+----------+----------+------------+---------------+ 

10 rows in set (0.00 sec) 


4. Write a SQL statement to increase the minimum and maximum salary of PU_CLERK by 2000 as well as the salary for those employees by 20% and commission percent by .10. 

  

  

  

  

  

Select. 

Salesman Table 
salesman_id               
name 
city 
commission
 
5001
James Hoog 
New York 
0.15 
5002 
Nail Knite 
Paris 
0.13 
5005 
Pit Alex 
London 
0.11 
5006 
Mc Lyon 
Paris 
0.14 
5003 

Lauson Hen 

0.12 
5007 
Paul Adam 
Rome 
0.13 
 

Orders Table 

ord_no 
purch_amt 
ord_date 
customer_id 
salesman_id
 
70001 
150.5 
05-10-2012 
3005 
5002 
70009 
270.65 
10-09-2012 
3001 
5005 
70002 
65.26 
05-10-2012 
3002 
5001 
70004 
110.5 
17-08-2012 
3009 
5003 
70007 
948.5 
10-09-2012 
3005 
5002 
70005 
2400.6 
27-07-2012 
3007 
5001 
70008 
5760 
10-09-2012 
3002 
5001 
70010 
1983.43 
10-10-2012 
3004 
5006 
70003 
2480.4 
10-10-2012 
3009 
5003 
70012 
250.45 
27-06-2012 
3008 
5002 
70011 
75.29 
17-08-2012 
3003 
5007 
70013 
3045.6 
25-04-2012 
3002 
5001 
  

Customers Table 
customer_id 
cust_name 
city 
grade  

salesman_id 
3002 
Nick Rimando 
New York 
100 
5001 
3005 
Graham Zusi 
California 
200 
5002 
3001 
Brad Guzan 
London 
5005 
3004 
Fabian Johns 
Paris 
300 
5006 
3007 
Brad Davis 
New York           
200 
5001 
3009 
Geoff Camero 
Berlin 
100 
5003 
3008 
Julian Green 
London 
300 
5002 
3003 
Jozy Altidor 

Moscow 
200 
5007 

 

  

  

  

Products Table 

PRO_ID                      
PRO_NAME 
PRO_PRICE 
PRO_COM 
101 

Mother Board 
3200 
15 
102 

Key Board 
450 
16 
103 

ZIP drive 
250 
14 
104 

Speaker 
550 
16 
105 

Monitor 
5000 
11 
106 

DVD drive      
900 
12 
107 

CD drive 
800 
12 
108 

Printer     
2600 
13 
109 

Refill cartridge 
350 
13 
110 
Mouse  
250 
12 


Departments table 

DEPARTMENT_ID 
DEPARTMENT_NAME 
MANAGER_ID 
LOCATION_ID 
10 

Administration 
200 
1700 
20 

Marketing    
201 
1800 
30 

Purchasing  
114 
1700 
40 

Human Resources  
204 
2400 
50 

Shipping     
11 
1500 
60 

IT    
103 
1400 
70 

Public Relations 
204 
2700 
80 

Sales 
145 
2500 
90 
Executive    
100 
1700 
100 
Finance 
108 
1700 
110 
Accounting 
205 
1700 

 
  

1.Write a SQL statement to display all the information of all salesmen 

mysql> select * from salesman; 
+-------------+------------+----------+------------+ 

| salesman_id | name       | city     | commission | 

+-------------+------------+----------+------------+ 

|        5001 | James Hoog | New York |       0.15 | 

|        5002 | Nail Knite | Paris    |       0.13 | 

|        5003 | Lauson Hen |          |       0.12 | 

|        5005 | Pit Alex   | London   |       0.11 | 

|        5006 | Mc Lyon    | Paris    |       0.14 | 

|        5007 | Paul Adam  | Rome     |       0.13 | 

+-------------+------------+----------+------------+ 

6 rows in set (0.24 sec) 


2.Write a SQL statement to display specific columns like name and commission for all the salesmen 

mysql> select name,commission from salesman; 
+------------+------------+ 

| name       | commission | 

+------------+------------+ 

| James Hoog |       0.15 | 

| Nail Knite |       0.13 | 

| Lauson Hen |       0.12 | 

| Pit Alex   |       0.11 | 

| Mc Lyon    |       0.14 | 

| Paul Adam  |       0.13 | 

+------------+------------+ 

6 rows in set (0.00 sec) 


3.Write a SQL statement to display names and city of salesman, who belongs to the city of Paris 

mysql> select name,city from salesman where city="Paris"; 
+------------+-------+ 

| name       | city  | 

+------------+-------+ 

| Nail Knite | Paris | 

| Mc Lyon    | Paris | 

+------------+-------+ 

2 rows in set (0.00 sec) 


4.Write a query to display the columns in a specific order like order date, salesman id, order number and purchase amount from for all the orders 

mysql> select ord_date,salesman_id,order_no,purch_amt from orders; 
+------------+-------------+----------+-----------+ 

| ord_date   | salesman_id | order_no | purch_amt | 

+------------+-------------+----------+-----------+ 

| 2012-10-05 |        5001 |     7000 |    265.26 | 

| 2012-10-15 |        5002 |    70001 |    150.50 | 

| 2012-10-10 |        5003 |    70003 |   2480.40 | 

| 2012-08-17 |        5003 |    70004 |    110.50 | 

| 2012-07-27 |        5001 |    70005 |   2400.60 | 

| 2012-09-10 |        5002 |    70007 |    948.50 | 

| 2012-09-10 |        5001 |    70008 |   5760.00 | 

| 2012-09-10 |        5005 |    70009 |    270.65 | 

| 2012-10-10 |        5006 |    70010 |   1983.43 | 

| 2012-08-17 |        5007 |    70011 |     75.29 | 

| 2012-06-27 |        5002 |    70012 |    250.45 | 

| 2012-04-25 |        5001 |    70013 |   3045.60 | 

+------------+-------------+----------+-----------+ 

12 rows in set (0.00 sec) 


5.Write a query which will retrieve the value of salesman id of all salesmen, getting orders from the customers in orders table without any repeats 

mysql> select distinct salesman_id from orders; 
+-------------+ 

| salesman_id | 

+-------------+ 

|        5001 | 

|        5002 | 

|        5003 | 

|        5005 | 

|        5006 | 

|        5007 | 

+-------------+ 

6 rows in set (0.04 sec) 


6. Write a SQL statement to display all the information for those customers with a grade of 200 

mysql> select * from customers where grade=200; 
+-------------+--------------+------------+-------+-------------+ 

| customer_id | cust_name    | city       | grade | salesman_id | 

+-------------+--------------+------------+-------+-------------+ 

|        3003 | Jozy Altidor | Moscow     |   200 |        5007 | 

|        3005 | Graham Zusi  | California |   200 |        5002 | 

|        3007 | Brad Davis   | New York   |   200 |        5001 | 

+-------------+--------------+------------+-------+-------------+ 

3 rows in set (0.12 sec) 


7. 1.Write a SQL query to find all the products with a price between Rs.200 and Rs.600 

  

Aggregation Function 

1.Write a SQL statement to find the total purchase amount of all orders. 

mysql> select sum(purch_amt) as sum from orders; 
+----------+ 

| sum      | 

+----------+ 

| 17741.18 | 

+----------+ 

1 row in set (0.04 sec) 


2.Write a SQL statement to find the average purchase amount of all orders. 

mysql> select avg(purch_amt) as avg_amount from orders; 
+-------------+ 

| avg_amount  | 

+-------------+ 

| 1478.431667 | 

+-------------+ 

1 row in set (0.00 sec) 


3.Write a SQL statement to find the number of salesmen currently listing for all of their customers 

mysql> select count(salesman_id) as no_of_employess from customers; 
+-----------------+ 

| no_of_employess | 

+-----------------+ 

|               8 | 

+-----------------+ 

1 row in set (0.04 sec) 


4.Write a SQL statement know how many customer have listed their names. 

mysql> select count(cust_name) as no_of_customers from customers; 
+-----------------+ 

| no_of_customers | 

+-----------------+ 

|               8 | 

+-----------------+ 

1 row in set (0.00 sec) 


5.Write a SQL statement to get the maximum purchase amount of all the orders 

mysql> select max(purch_amt) from orders; 
+----------------+ 

| max(purch_amt) | 

+----------------+ 

|        5760.00 | 

+----------------+ 

1 row in set (0.06 sec) 

  

Relational Operator 

1.Write a query to display all customers with a grade above 100 

mysql> select * from customers where grade>100; 
+-------------+--------------+------------+-------+-------------+ 

| customer_id | cust_name    | city       | grade | salesman_id | 

+-------------+--------------+------------+-------+-------------+ 

|        3003 | Jozy Altidor | Moscow     |   200 |        5007 | 

|        3004 | Fabian Johns | Paris      |   300 |        5006 | 

|        3005 | Graham Zusi  | California |   200 |        5002 | 

|        3007 | Brad Davis   | New York   |   200 |        5001 | 

|        3008 | Julian Green | London     |   300 |        5002 | 

+-------------+--------------+------------+-------+-------------+ 

5 rows in set (0.03 sec) 


2.Write a query statement to display all customers in New York who have a grade value above 100 

mysql> select * from customers where city="new york" and grade>100; 
+-------------+------------+----------+-------+-------------+ 

| customer_id | cust_name  | city     | grade | salesman_id | 

+-------------+------------+----------+-------+-------------+ 

|        3007 | Brad Davis | New York |   200 |        5001 | 

+-------------+------------+----------+-------+-------------+ 

1 row in set (0.00 sec) 


3.Write a SQL statement to display all customers, who are either belongs to the city New York or had a grade above 100 

mysql> select * from customers where city="new york" or grade>100; 
+-------------+--------------+------------+-------+-------------+ 

| customer_id | cust_name    | city       | grade | salesman_id | 

+-------------+--------------+------------+-------+-------------+ 

|        3002 | Nick Rimando | New York   |   100 |        5001 | 

|        3003 | Jozy Altidor | Moscow     |   200 |        5007 | 

|        3004 | Fabian Johns | Paris      |   300 |        5006 | 

|        3005 | Graham Zusi  | California |   200 |        5002 | 

|        3007 | Brad Davis   | New York   |   200 |        5001 | 

|        3008 | Julian Green | London     |   300 |        5002 | 

+-------------+--------------+------------+-------+-------------+ 

6 rows in set (0.00 sec) 


4.Write a SQL statement to display either those orders which are not issued on date 2012-09-10 and issued by the salesman whose ID is 5005 and below or those orders which purchase amount is 1000.00 and below. 

mysql> select * from orders where not(ord_date="2012-09-10") and salesman_id<=5005 or purch_amt=1000; 
+----------+-----------+------------+-------------+-------------+ 

| order_no | purch_amt | ord_date   | customer_id | salesman_id | 

+----------+-----------+------------+-------------+-------------+ 

|     7000 |    265.26 | 2012-10-05 |        3002 |        5001 | 

|    70001 |    150.50 | 2012-10-15 |        3005 |        5002 | 

|    70003 |   2480.40 | 2012-10-10 |        3009 |        5003 | 

|    70004 |    110.50 | 2012-08-17 |        3009 |        5003 | 

|    70005 |   2400.60 | 2012-07-27 |        3007 |        5001 | 

|    70012 |    250.45 | 2012-06-27 |        3008 |        5002 | 

|    70013 |   3045.60 | 2012-04-25 |        3002 |        5001 | 

+----------+-----------+------------+-------------+-------------+ 

7 rows in set (0.06 sec) 

  

  

SORTING and FILTERING 

1.Write a query in SQL to display the full name (first and last name), and salary for those employees who earn below 6000 
2.Write a query in SQL to display the first and last name, and department number for all employees whose last name is "Ernst". //join 
3.Write a query in SQL to display the full name (first and last),  salary, and department number for those employees whose first name does not containing the letter M and make the result set in ascending order by department number 
4.Write a query in SQL to display the full name (first and last name), and salary for all employees who does not earn any commission 

  
Subqueries 

1.Write a query to display all the orders from the orders table issued by the salesman 'Paul Adam' 

mysql> select * from orders where salesman_id=(select salesman_id from salesman where name="paul adam"); 
+----------+-----------+------------+-------------+-------------+ 

| order_no | purch_amt | ord_date   | customer_id | salesman_id | 

+----------+-----------+------------+-------------+-------------+ 

|    70011 |     75.29 | 2012-08-17 |        3003 |        5007 | 

+----------+-----------+------------+-------------+-------------+ 

1 row in set (0.06 sec) 

2.Write a query to display all the orders for the salesman who belongs to the city London 

mysql> select * from orders where salesman_id=(select salesman_id from salesman where city="london"); 
+----------+-----------+------------+-------------+-------------+ 

| order_no | purch_amt | ord_date   | customer_id | salesman_id | 

+----------+-----------+------------+-------------+-------------+ 

|    70009 |    270.65 | 2012-09-10 |        3001 |        5005 | 

+----------+-----------+------------+-------------+-------------+ 

1 row in set (0.00 sec) 

3. Write a query to find all the orders issued against the salesman who works for customer whose id is 3007 

mysql> select * from orders where customer_id=3007; 
+----------+-----------+------------+-------------+-------------+ 

| order_no | purch_amt | ord_date   | customer_id | salesman_id | 

+----------+-----------+------------+-------------+-------------+ 

|    70005 |   2400.60 | 2012-07-27 |        3007 |        5001 | 

+----------+-----------+------------+-------------+-------------+ 

1 row in set (0.03 sec) 

4.Write a query to display the commission of all the salesmen servicing customers in Paris 

mysql> select salesman_id,commission from salesman where city="paris"; 
+-------------+------------+ 

| salesman_id | commission | 

+-------------+------------+ 

|        5002 |       0.13 | 

|        5006 |       0.14 | 

+-------------+------------+ 
2 rows in set (0.00 sec) 

  

 

Joins 

1.Write a query in SQL to display the first name, last name, department number, and department name for each employee 
2.Write a query in SQL to display the first name, last name, department number and department name, for all employees for departments 80 or 40 
3.Write a query in SQL to display the first name of all employees including the first name of their manager 
4.Write a query in SQL to display all departments including those where does not have any employee 
5.Write a query in SQL to display the first name, last name, department number and name, for all employees who have or have not any department 
Bank management System 

 
1.Write a query to display account number, customer’s number, customer’s firstname,lastname,account opening date. 
Display the records sorted in ascending order based on account number. 

SELECT account_number,am.customer_number,firstname,lastname,account_opening_date 
FROM customer_master cm INNER JOIN account_master am 
ON cm.customer_number=am.customer_number 
ORDER BY account_number 

  
2.Write a query to display the numberof customer’s from Delhi. Give the count an alias name of Cust_Count. 

SELECT count(customer_number) Cust_Count 
FROM customer_master 
WHERE customer_city='Delhi' 

  
3. Write a query to display  the customer number, customer firstname,account number for the customer’s whose accounts were created after 15th of any month. 
Display the records sorted in ascending order based on customer number. 

SELECT  am.customer_number, firstname, account_number 
FROM customer_master cm INNER JOIN account_master am 
ON cm.customer_number=am.customer_number 
WHERE extract(day from account_opening_date)>15 
ORDER BY am.customer_number 
  

4. Write a query to display customer number, customer's first name, account number where the account status is terminated. 
Display the records sorted in ascending order based on customer number. 

SELECT am.customer_number,firstname, account_number 
FROM customer_master cm INNER JOIN account_master am 
ON cm.customer_number=am.customer_number 
WHERE account_status='Terminated' 
ORDER BY am.customer_number. 
  

5. Write a query to display the number of customers who have registration but no account in the bank. 

Give the alias name as Count_Customer for number of customers. 
SELECT count(customer_number) Count_Customer 
FROM customer_master 
WHERE customer_number NOT IN (SELECT customer_number FROM account_master) 

  
6. Write  a query to display the firstname of the customers who have more than 1 account. Display the records in sorted order based on firstname. 

Select firstname 
FROM customer_master cm INNER JOIN account_master am 
ON cm.customer_number=am.customer_number 
group by firstname 
having count(account_number)>1 
order by firstname; 
  

7. Write a query to display the number of clients who have asked for loans but they don’t have any account in the bank though they are registered customers. Give the count an alias name of Count. 

SELECT count(ld.customer_number) Count 
FROM customer_master cm INNER JOIN loan_detailsld 
ON  cm.customer_number=ld.customer_number 
WHERE cm.customer_number NOT IN ( SELECTcustomer_number FROM account_master) 
  

8. Write a query to show the branch name,branch city where we have the maximum customers. 
For example the branch B00019 has 3 customers, B00020 has 7 and B00021 has 10. So branch id B00021 is having maximum customers. If B00021 is Koramangla branch Bangalore, Koramangla branch should be displayed along with city name Bangalore. 
In case of multiple records, display the records sorted in ascending order based on branch name. 

selectbranch_name,branch_city 
FROM branch_master INNER JOIN account_master 
ON branch_master.branch_id=account_master.branch_id 
group by branch_name 

  
9. Write a query to display the customer’s firstname who have multiple accounts (atleast  2 accounts).  Display the records sorted in ascending order based on customer's firstname. 

SELECT firstname 
FROM customer_master INNER JOIN account_master 
ON customer_master.customer_number=account_master.customer_number 
GROUP BY firstname 
having count(firstname)>=2 order by firstname; 
  

10. Write a query to display account id, customer’s firstname, customer’slastname for the customer’s whose account is Active. 
Display the records sorted in ascending order based on account id /account number.

SELECT account_number, firstname, lastname 
FROMcustomer_master cm INNER JOIN account_master am 
ON cm.customer_number=am.customer_number 
WHERE account_status='Active' 
ORDER BY account_number. 

  

Jdbc Exercise(Using Statement and PreparedStatement) 

1.Let’s write code to insert a new record into the table Users with following details: 
username: steve 
password: secretpass 
fullname: steve paul 
email: steve.paul@hcl.com 
2. Select all records from the Users table and print out details for each record. 
3. Write a code to update all the details  of “steve paul”. 
4. Write a code to delete a record whose username field contains “steve”. 
Jdbc Transaction Management 
1.Create a table named customer including name,salary,email. 
2.One of the constraints on the table is that email has to be unique. If we enter same email a second time to violate this constraint. It results in SQL exception. Have to  rollback transaction programmatically in exception handling block. 
Sample Input and Output: 
If Email is unique then data should save successfully. 
Enter name 
revathi 
Enter salary 
10000 
Enter email 
a@hcl.com 
Want to add more records y/n 
y 
Enter name 
drishnaa 
Enter salary 
20000 
enter email 
b@hcl.com 
Want to add more records y/n 
n 
Data Saved Successfully 
If Email is  violated. 
Enter name 
revathi 
Enter salary 
10000 
Enter email 
a@hcl.com 
Email Id must be unique . Data Rollback successfully. 

 