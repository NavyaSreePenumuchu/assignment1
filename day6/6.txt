1.Write a program to assign the current thread to t1. Change the name of the thread to MyThread. 
Display the changed name of the thread. Also it should display the current time. Put the thread to sleep for 10 seconds and display the time again.  

Solution: 

package assessment6;

import java.time.LocalTime; 
class ThreadDemo extends Thread{ 
public void run() { 
System.out.print("The default name "); 
System.out.println(Thread.currentThread().getName()); 
Thread.currentThread().setName("t1"); 
System.out.println(Thread.currentThread().getName()); 
Thread.currentThread().setName("MyThread"); 
System.out.println(Thread.currentThread().getName());    
System.out.println(LocalTime.now()); 
try { 
Thread.sleep(10000); 
} catch (InterruptedException e) { 
e.printStackTrace(); 
} 
System.out.println(LocalTime.now()); 
} 
} 
public class BasicThreadOps { 
public static void main(String[] args) { 
ThreadDemo thread1=new ThreadDemo(); 
thread1.start(); 
} 
} 

 

2.In the previous program remove the try{}catch(){} block surrounding the sleep method and try to execute the code. What is your observation?  

Solution: 

Exception in thread "Thread-0" java.lang.Error: Unresolved compilation problem:  

Unhandled exception type InterruptedException  
at assessmet6.ThreadDemo.run(BasicThreadOps.java:14) 
A checked exception is thrown called InterruptedException. 

3. Write a program to create a class DemoThread1 implementing Runnable interface. In the constructor, create a new thread and start the thread. In run() display a message "running child Thread in loop : " display the value of the counter ranging from 1 to 10. Within the loop put the thread to sleep for 2 seconds. In main create 3 objects of the DemoTread1 and execute the program.  

Solution: 

package assessment6; 
public class DemoThread1 implements Runnable{ 
public void run() { 
for (int counter=1;counter<=10;counter++) { 
System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter); 
try { 
Thread.sleep(2000); 
} catch (InterruptedException e) {  
e.printStackTrace(); 
} 
} 
} 
public static void main(String[] args) { 
DemoThread1 dt1=new DemoThread1(); 
Thread th1=new Thread(dt1); 
Thread th2=new Thread(dt1); 
Thread th3=new Thread(dt1); 
th1.start(); 
      th2.start(); 
th3.start(); 
} 
} 

 
4.Rewrite the earlier program so that, now the class DemoThread1 instead of implementing from Runnable interface, will now extend from Thread class. 

 Solution: 

package assessment6;  
public class DemoThread1 extends Thread { 
public DemoThread1(){ 
start(); 
} 
public void run() { 
for (int counter=1;counter<=10;counter++) { 
System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter); 
try { 
Thread.sleep(2000); 
} catch (InterruptedException e) { 
e.printStackTrace(); 
} 
} 
} 
@SuppressWarnings("unused") 
public static void main(String[] args) { 
new DemoThread1(); 
new DemoThread1(); 
new DemoThread1(); 
} 
} 

 

5.Write a program to create a class Number  which implements Runnable. Run method displays the multiples of a number accepted as a parameter. 
In main create three objects - first object should display the multiples of 2, second should display the multiples of 5 and third should display the multiples of 8.
 Display appropriate message at the beginning and ending of thread. The main thread should wait for the first object to complete. 
Display the status of threads before the multiples are displayed and after completing the multiples.  

Solution: 

package assessment6; 
public class Number  implements Runnable{ 
public int input; 
    Number(int input){ 
    	this.input=input; 
    } 
public static synchronized void display(int input) { 
} 
    public static void main(String[] args) throws InterruptedException { 
        Number thread1=new Number(2); 
        Number thread2=new Number(5); 
        Number thread3=new Number(8); 
        Thread t1=new Thread(thread1); 
        t1.start(); 
        t1.join(); 
        Thread t2=new Thread(thread2); 
        t2.start(); 
        t2.join(); 
        Thread t3=new Thread(thread3); 
        t3.start(); 
        t3.join(); 
    } 

    @Override 
    public synchronized void run() { 
    System.out.println(Thread.currentThread().getName()+" is" +Thread.currentThread().isAlive()); 
        System.out.println("Beginning of "+input+" multiples:"); 
        for(int i=1;i<=10;i++) { 
            try { 
Thread.sleep(500); 
} catch (InterruptedException e) {  
e.printStackTrace(); 
} 
                System.out.println(i*input); 
        }
        System.out.println("End of "+input+" multiples:"); 
        System.out.println(Thread.currentThread().getName()+" is dead"); 
    } 
} 

 