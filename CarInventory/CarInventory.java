import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;
class CarDetails{
	String make;
	String model;
	int year;
	float sales_price;
	public CarDetails(String make, String model, int year, float sales_price) {
		this.make = make;
		this.model = model;
		this.year = year;
		this.sales_price = sales_price;
	}
	public String getMake() {
		return make;
	}
	public String getModel() {
		return model;
	}
	public int getYear() {
		return year;
	}
	public float getSales_price() {
		return sales_price;
	}

	public String toString() {
		return  year+"  "+make+"  "+model+"  $"+sales_price;
	}
}

public class CarInventory {
	static Connection con=null;
	public static void main(String[] args) throws Exception {


		Statement st=null;

		try {
			con=DBConnection.getConnection();
			st = con.createStatement();
			// st.execute("create table car(id varchar(30) primary key,make varchar(30),model varchar(30),year int,sales_price varchar(30))");
			Scanner sc = new Scanner(System.in);
			// printing services available
			print_services();
			//reading commands until user enter quit commands
			while(true) {
				System.out.print("Enter command : ");
				String command = sc.nextLine();
				if(command.equals("quit")) {
					break;
				}else {
					ResultSet rs= st.executeQuery("select count(*) from car");
					rs.next();
					int count = rs.getInt(1);
					get_commands(command,count);
				}
			}
			System.out.println("Good Bye!");


		}
		catch (Exception e) {
		  e.printStackTrace();
		}
		//		  finally
		//		  {
		//			  st.close();
		//			  con.close();
		//		  }

	}
	private static void print_services() {
		System.out.println("Hello!");
		System.out.println("Welcome to Mullet Joe's Gently Used Autos!");
		System.out.println("The commands are ");
		System.out.println("add ");
		System.out.println("list");
        System.out.println("update");
        System.out.println("delete");
		System.out.println("quit ");
	}

	private static void get_commands(String command,int count) throws SQLException {
		Statement st = con.createStatement();
		if(command.equals("add")) {
			try {
				if(count<20) {
					Scanner sc = new Scanner(System.in);
					System.out.println("Make : ");
					String make = sc.nextLine();
					System.out.println("Model : ");
					String model = sc.nextLine();
					System.out.println("Id :");
                    int id =sc.nextInt();
					System.out.println("Year : ");
					int year = sc.nextInt();
					System.out.println("Sales Price ($) :");
					float sales_price = sc.nextFloat();
					CarDetails cd  = new CarDetails(make,model,year,sales_price);
                    String q = "INSERT INTO car (id, make, model,year,sales_price)" + "values("+id+",'"+cd.getMake()+"','"+cd.getModel()+"',"+cd.getYear()+","+cd.getSales_price()+")";
					System.out.println(q);
					int noOfRows=st.executeUpdate(q);
					System.out.println(noOfRows+" inserted!! ");
				}else {
					System.out.println("Limit Reached!!");
				}

			}catch(InputMismatchException e) {
				System.out.println("Please enter valid Details");
			}

		}else if(command.equals("list")) {
			// checking catalog is empty of not
			if(count==0) {
				System.out.println("There are currently no cars in the catalog.");
			}else {
				int total = 0;
				ResultSet rs = st.executeQuery("select * from car");
				while(rs.next()) {
					String make = rs.getString("make");
					String model = rs.getString("model");
					int year = Integer.parseInt(rs.getString("year"));
					int sales = (int)(rs.getFloat("sales_price"));
					total+=sales;
					System.out.println(year+"  "+make+"  "+model+"  $"+sales);
				}
				System.out.println("Number of cars "+count);
				System.out.println("Total inventory :  $"+total);


			}
		}else if(command.equals("delete")) {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter id to delete from the list");
			int id=sc.nextInt();
			st.executeUpdate("delete from car where id="+id);
			System.out.println("Record deleted successfully");
		}else if(command.equals("update")) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter the ID of the car you want to update");
			String id_update=sc.nextLine();
			System.out.println("Enter the field you want to update");
			String model_select=sc.nextLine();
			if(model_select.equals("model")){
				try {
					System.out.println("Enter the new model of the car to update");
					String new_model=sc.nextLine();
					int noOfRowsUpdated=st.executeUpdate("update car set model='"+new_model+"' where id='"+id_update+"'");
					System.out.println(noOfRowsUpdated+" rows updated");
				}
				catch(Exception e) {
			       e.printStackTrace();
				}
			}
			else if(model_select.equals("year")) {
				System.out.println("Enter the new year");
				int year=Integer.parseInt(sc.nextLine());
				int noOfRowsUpdated=st.executeUpdate("update car set year='"+year+"' where id='"+id_update+"'");
				System.out.println(noOfRowsUpdated+" rows updated");
			}
			else if(model_select.equals("make")) {
				System.out.println("Enter the new make");
				String new_make=sc.nextLine();
				int noOfRowsUpdated=st.executeUpdate("update car set make='"+new_make+"' where id='"+id_update+"'");
				System.out.println(noOfRowsUpdated+" rows updated");
			}
			else if(model_select.equals("price")) {
				try {
					System.out.println("Enter the new price");
					double price=Double.parseDouble(sc.nextLine());
					int noOfRowsUpdated=st.executeUpdate("update car set sales_price='"+price+"' where id='"+id_update+"'");
					System.out.println(noOfRowsUpdated+" rows updated");
				}
				catch(Exception e) {
					e.printStackTrace();
				}


			}
			else {
				System.out.println("Sorry, but "+command+" is not a valid command. Please try again.");
			}
		}

	}
}
