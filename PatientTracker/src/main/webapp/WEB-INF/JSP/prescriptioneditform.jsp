<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.view{
border-collapse:collapse;
color:cyan;
}
</style>
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
<table border="1" class="view">
<form:form action="/PatientTracker/editSavePrescription">
<tr><td><form:hidden path="presid" /></td></tr>
<tr><th><form:label path="name">Name</form:label></th>
<td><form:input path="name"/></td></tr>
<tr><th><form:label path="age">Age</form:label></th>
<td><form:input path="age" /></td></tr>
<tr><th><form:label path="weight">Weight</form:label></th>
<td><form:input path="weight" /></td></tr>
<tr><th><form:label path="gender">Gender</form:label></th>
<td><form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female </td></tr>
<tr><th><form:label path="Medicinename">MedicineName</form:label></th>
<td><form:input path="medicinename" /></td></tr>
<tr><th><form:label path="take">Take</form:label></th>
<td><form:input path="take" /></td></tr>
<tr><th><form:label path="drugform">DrugForm</form:label></th>
<td><form:input path="drugform" /></td></tr>
<tr><th><form:label path="route">Route</form:label></th>
<td><form:input path="route" /></td></tr>
<tr><th><form:label path="duration">Duration</form:label></th>
<td><form:input path="duration" /></td></tr>
<tr><td>
<input type="submit" value="Save Prescription" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
</form:form>
</table>
 
 </body>
</html>