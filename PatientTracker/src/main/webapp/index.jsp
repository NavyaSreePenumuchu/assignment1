<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<!-- <nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add Patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">View Medicine Details</a>
      </li> 
        <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li> 
      
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">View Prescription Details</a>
      </li>   
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add Prescription</a>
      </li>  
    </ul>
  </div>  
</nav> -->
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 <div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav mr-auto">
 <li class="nav-item dropdown">
 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          List
        </a>
        <div class="dropdown-menu pull-right" aria-labelledby="navbarDropdown" >
          <a class="dropdown-item" href="patient">Add Patient</a>
          <a class="dropdown-item" href="patientdetails">View patient Details</a>
          <a class="dropdown-item" href="medicine">Add Medicine</a>
          <a class="dropdown-item" href="medicinedetails">View Medicine Details</a>
          <a class="dropdown-item" href="prescription">Add Prescription</a>
          <a class="dropdown-item" href="prescriptiondetails">View Prescription Details</a>
        </div>
      </li>
      </ul></div></nav>

<%-- <h3 >Welcome ${name}</h3> --%>
<!-- <a href="viewClerkDetails">View Clerk Details</a><br>
<a href="addClerk">Add Clerk</a><br>
<a href="viewDoctorDetails">View Doctor Details</a><br>
<a href="addDoctor">Add Doctor</a> -->
</body>
</html>