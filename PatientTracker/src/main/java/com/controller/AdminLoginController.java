package com.controller;


	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestParam;
	import org.springframework.web.servlet.ModelAndView;

	import com.service.AdminRegisterServiceIntf;

	
	@Controller
	public class AdminLoginController {
	    @Autowired
	    AdminRegisterServiceIntf service;
	    @RequestMapping("/AdminLogin")
	    public ModelAndView adminLogin() {
	        return new ModelAndView("loginform");
	        
	    }
	    @RequestMapping("/saveLogin")
	    public ModelAndView checkAdmin(@RequestParam("adminid") int  adminid,@RequestParam("password") String password )
	        {
	            
	        boolean status=    service.checkAdmin(adminid,password);
	        if(status)
	        {
	            return new ModelAndView("homepage");
	        }
	        else
	        {
	            System.out.println("Invalid");
	            return new ModelAndView("loginpage");
	            
	        }
	        
	
	}


}
