package com.dao;

import java.util.List;


import com.model.Prescription;

public interface PrescriptionDaoIntf {
	

	public void savePrescription(Prescription prescription);

	public List<Prescription> prescriptionDetails();
	public int deletePrescriptionById(int presid);
	public List<Prescription> viewPrescriptionById(int presid);
	Prescription getPrescriptionById(int presid);
	void updatePrescription(Prescription prescription);

}
