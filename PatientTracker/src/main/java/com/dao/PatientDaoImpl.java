package com.dao;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Patient;
@Repository
	public class PatientDaoImpl implements PatientDaoIntf{

	
	    @Autowired
	    SessionFactory sessionFactory;
	    public void savePatient(Patient patient) {
	        sessionFactory.openSession().save(patient);
	        
	    }
		
	    public List<Patient> patientDetails() {
	        List<Patient> li=(List<Patient>) sessionFactory.openSession().createQuery("from Patient").list();
	        return li;
	    }
	    
	    public int deletePatientById(int patientid) {
	        int result=0;
	        Patient patient=(Patient) sessionFactory.openSession().load(Patient.class, patientid);   
	        if(patient!=null) {
	            Query query=(Query) sessionFactory.openSession().createQuery("delete from Patient p where p.patientid=:patientid");
	            query.setParameter("patientid",patientid);
	             result=query.executeUpdate();   
	        }
	        return result;
	    }
	   		@Override
		public List<Patient> viewPatientById(int patientid) {
			Query query= (Query) sessionFactory.openSession().createQuery("from Patient p where p.patientid=?");
	        query.setParameter(0,patientid);
	        List<Patient> li=query.list();
	            return li;
		}
	   		public Patient getPatientById(int patientid) {
	   			SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from patient_info where patientid=:patientid").addEntity(Patient.class);
	   			query.setParameter("patientid",patientid);
	   			Patient patient= (Patient) query.uniqueResult();
	   			return patient;
	   		}

	   		
	   		public void updatePatient(Patient patient) {
	   			SQLQuery query=sessionFactory.openSession().createSQLQuery("update patient_info set disease=:disease,firstname=:name,lastname=:lname,age=:age,gender=:gender,address=:address,mobileno=:number where patientid=:patientid").addEntity(Patient.class);
	   			query.setParameter("disease",patient.getDisease());
	   			query.setParameter("name",patient.getFirstname());
	   			query.setParameter("lname",patient.getLastname());
	   			query.setParameter("age", patient.getAge());
	   			query.setParameter("gender", patient.getGender());
	   			query.setParameter("address",patient.getAddress());
	   			query.setParameter("number",patient.getMobileno());
	   			query.setParameter("patientid",patient.getPatientid());
	   			query.executeUpdate();		
	   		}
			}

