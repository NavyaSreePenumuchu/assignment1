package com.dao;

import com.model.AdminRegister;

public interface AdminRegisterDaoIntf {
	void saveAdminRegister(AdminRegister register);

	boolean checkAdmin(int adminid, String password);


}
