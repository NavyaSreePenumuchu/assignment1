package com.dao;

import java.util.List;

import com.model.Patient;


public interface PatientDaoIntf {


	public void savePatient(Patient patient);

	public List<Patient> patientDetails();

	public int deletePatientById(int patientid);

	public List<Patient> viewPatientById(int patientid);
	Patient getPatientById(int patientid);

	void updatePatient(Patient patient);

}
