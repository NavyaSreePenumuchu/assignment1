package com.service;

import java.util.List;


import com.model.Patient;


public interface PatientServiceIntf {


	void savePatient(Patient patient);
	List<Patient> patientDetails();
	int deletePatientById(int patientid);
	List<Patient> viewPatientById(int patientid);
	Patient getPatientById(int patientid);
	void updatePatient(Patient patient);


}
