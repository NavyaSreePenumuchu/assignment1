package com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;


	@Entity
	@Table(name="medicine_details")
	public class Medicine {
		 @Id
		 @GeneratedValue(strategy=GenerationType.IDENTITY)
		 private int medicineid;
		 @NotEmpty(message="medicinename is required")
		 private String medicinename;
		@NotEmpty(message="medicineusage is required")
		  private String medicineusage;
		  private float medicinecost;
		 
		  
		public int getMedicineid() {
			return medicineid;
		}
		public void setMedicineid(int medicineid) {
			this.medicineid = medicineid;
		}
		public String getMedicinename() {
			return medicinename;
		}
		public void setMedicinename(String medicinename) {
			this.medicinename = medicinename;
		}
		public String getMedicineusage() {
			return medicineusage;
		}
		public void setMedicineusage(String medicineusage) {
			this.medicineusage = medicineusage;
		}
		public float getMedicinecost() {
			return medicinecost;
		}
		public void setMedicinecost(float medicinecost) {
			this.medicinecost = medicinecost;
		}
		/*public Date getMnfdate() {
			return mnfdate;
		}
		public void setMnfdate(Date mnfdate) {
			this.mnfdate = mnfdate;
		}
		public Date getExpdate() {
			return expdate;
		}
		public void setExpdate(Date expdate) {
			this.expdate = expdate;
		}*/
		  

	    
}
