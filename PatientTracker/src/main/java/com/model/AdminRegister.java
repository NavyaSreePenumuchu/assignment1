package com.model;


	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.Table;

	import org.hibernate.validator.constraints.NotEmpty;

	/*import lombok.AllArgsConstructor;
	import lombok.Data;
	import lombok.NoArgsConstructor;

	@NoArgsConstructor
	@AllArgsConstructor*/

	@Entity
	@Table(name="admin_register")
	public class AdminRegister {
		/* @NotEmpty(message="not empty") */
		private String firstname;
		private String lastname;
		private int age;
		private String gender;
		private String contactnumber;
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int adminid;
		private String password;
		
		
		
		public AdminRegister(String firstname, String lastname, int age, String gender, String contactnumber,
				int adminid, String password) {
			super();
			this.firstname = firstname;
			this.lastname = lastname;
			this.age = age;
			this.gender = gender;
			this.contactnumber = contactnumber;
			this.adminid = adminid;
			this.password = password;
		}
		public AdminRegister() {
			super();
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getLastname() {
			return lastname;
		}
		public void setLastname(String lastname) {
			this.lastname = lastname;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getContactnumber() {
			return contactnumber;
		}
		public void setContactnumber(String contactnumber) {
			this.contactnumber = contactnumber;
		}
		public int getAdminid() {
			return adminid;
		}
		public void setAdminid(int adminid) {
			this.adminid = adminid;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}

	}


