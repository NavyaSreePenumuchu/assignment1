package com.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;


	@Entity
	@Table(name="Prescription_info")
	public class Prescription {
		 @Id
		 @GeneratedValue(strategy=GenerationType.IDENTITY)
		 private int presid;
		 
		@NotEmpty(message="name is required")
		 private String name;
			  private int age;
			  @NotEmpty(message="Gender is required")
			  private String gender;
			  private int weight;
			 @NotEmpty(message="Medicinename is required")
			  private String medicinename;
			 @Min(value=2,message="Dose should not be less than 2 times")
			  private int take;
			  @NotEmpty(message="Drugform is required")
			  private String drugform;
			  @NotEmpty(message="Route is required")
			  private String route;
			  @Min(value=1,message="duration should not be less than 1 day")
			  private int duration;
			  
			  
			public Prescription() {
				super();
			}
			public Prescription(int presid, Date date, String name, int age, int weight, String medicinename, int take,
					String drugform, String route, int duration) {
				super();
				this.presid = presid;
				/*this.date = date;*/
				this.name = name;
				this.age = age;
				this.weight = weight;
				this.medicinename = medicinename;
				this.take = take;
				this.drugform = drugform;
				this.route = route;
				this.duration = duration;
			}
			public int getPresid() {
				return presid;
			}
			public void setPresid(int presid) {
				this.presid = presid;
			}
			/*public Date getDate() {
				return date;
			}
			public void setDate(Date date) {
				this.date = date;}*/
			
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public int getAge() {
				return age;
			}
			public void setAge(int age) {
				this.age = age;
			}
			public String getGender() {
				return gender;
			}
			public void setGender(String gender) {
				this.gender = gender;
			}
			public int getWeight() {
				return weight;
			}
			public void setWeight(int weight) {
				this.weight = weight;
			}
			public String getMedicinename() {
				return medicinename;
			}
			public void setMedicinename(String medicinename) {
				this.medicinename = medicinename;
			}
			public int getTake() {
				return take;
			}
			public void setTake(int take) {
				this.take = take;
			}
			public String getDrugform() {
				return drugform;
			}
			public void setDrugform(String drugform) {
				this.drugform = drugform;
			}
			public String getRoute() {
				return route;
			}
			public void setRoute(String route) {
				this.route = route;
			}
			public int getDuration() {
				return duration;
			}
			public void setDuration(int duration) {
				this.duration = duration;
			}
			  
			  
}
