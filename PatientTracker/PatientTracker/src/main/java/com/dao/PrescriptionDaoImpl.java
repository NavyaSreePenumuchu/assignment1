package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.model.Prescription;


	@Repository
	public class PrescriptionDaoImpl  implements PrescriptionDaoIntf{

	
	    @Autowired
	    SessionFactory sessionFactory;
	    public void savePrescription(Prescription prescription) {
	        sessionFactory.openSession().save(prescription);
	        
	    }
		
	  public List<Prescription> prescriptionDetails() {
	        List<Prescription> li=(List<Prescription>) sessionFactory.openSession().createQuery("from Prescription").list();
	        return li;
	    }
	  public int deletePrescriptionById(int presid) {
	        int result=0;
	        Prescription prescription=(Prescription) sessionFactory.openSession().load(Prescription.class, presid);   
	        if(prescription!=null) {
	            Query query=(Query) sessionFactory.openSession().createQuery("delete from Prescription p where p.presid=:presid");
	            query.setParameter("presid",presid);
	             result=query.executeUpdate();   
	        }
	        return result;
	    }
	  @Override
		public List<Prescription> viewPrescriptionById(int presid) {
			Query query= (Query) sessionFactory.openSession().createQuery("from Prescription p where p.presid=?");
	        query.setParameter(0,presid);
	        List<Prescription> li=query.list();
	            return li;
		}
	  public Prescription getPrescriptionById(int presid) {
			SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from prescription_info where presid=:presid").addEntity(Prescription.class);
			query.setParameter("presid",presid);
			Prescription prescription= (Prescription) query.uniqueResult();
			return prescription;
		}

		
		public void updatePrescription(Prescription prescription) {
			SQLQuery query=sessionFactory.openSession().createSQLQuery("update prescription_info set name=:name,age=:age,weight=:weight,gender=:gender,medicinename=:medicinename,take=:take,drugform=:drugform,route=:route,duration=:duration where presid=:presid").addEntity(Prescription.class);
			query.setParameter("name",prescription.getName());
			query.setParameter("age", prescription.getAge());
			query.setParameter("weight",prescription.getWeight());
			query.setParameter("gender", prescription.getGender());
			query.setParameter("medicinename",prescription.getMedicinename());
			query.setParameter("take", prescription.getTake());
			query.setParameter("drugform",prescription.getDrugform());
			query.setParameter("route",prescription.getRoute());
			query.setParameter("duration",prescription.getDuration());
			query.setParameter("presid",prescription.getPresid());
			query.executeUpdate();		
		}

		
	}

