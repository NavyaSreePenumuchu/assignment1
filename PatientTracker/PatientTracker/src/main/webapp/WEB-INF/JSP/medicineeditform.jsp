<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.view{
border-collapse:collapse;
color:cyan;
}
</style>
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
<table border="1" class="view">
<form:form action="/PatientTracker/editSaveMedicine">
<tr><td><form:hidden path="medicineid" /></td></tr>
<tr><th><form:label path="medicinename">MedicineName</form:label></th>
<td><form:input path="medicinename"/></td></tr>
<tr><th><form:label path="medicineusage">MedicineUsage</form:label></th>
<td><form:input path="medicineusage" /></td></tr>
<tr><th><form:label path="medicinecost">MedicineCost</form:label></th>
<td><form:input path="medicinecost" /></td></tr>
<tr><td>
<input type="submit" value="Save Medicine" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
</form:form>
</table>
 
 </body>
</html>