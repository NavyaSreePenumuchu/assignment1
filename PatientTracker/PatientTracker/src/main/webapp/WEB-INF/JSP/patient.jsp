<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Tracker</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
<style type="text/css">
.view{​​​​​​​
border-collapse:collapse;
background-color:#ffffff00;
}
.bg-img{
background-image:url("E:/Pics/DocPat.jpeg");
}​​​​​​​
</style>
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Add Patient</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>


 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
     <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<div class="bg-img">
<table border="1" width="500" height="200">
<form:form action="savePatient" modelAttribute="patient" method="post">
<%-- Disease
<form:input path="disease" size="30"/><form:errors cssClass="error" path="disease"/><br/><br> --%>
<tr><th><form:label path="disease">Disease</form:label></th>
<td><form:input path="disease"/></td></tr>
<%-- First name:
<td><form:input path="firstname" size="30"/><form:errors cssClass="error" path="firstname"/><br/><br> --%>
<tr><th><form:label path="firstname">FirstName</form:label></th>
<td><form:input path="firstname"/></td></tr>
<%-- Last name:
<form:input path="lastname" size="30"/><form:errors cssClass="error" path="lastname"/><br/><br> --%>
<tr><th><form:label path="lastname">LastName</form:label></th>
<td><form:input path="lastname"/></td></tr>
<%-- Age:
<form:input path="age" size="30"/><form:errors cssClass="error" path="age"/><br/><br> --%>
<tr><th><form:label path="age">Age</form:label></th>
<td><input type="text" name="age" /></td></tr>
<%-- Gender:
Male:<form:radiobutton path="gender" value="Male"/>
Female:<form:radiobutton path="gender" value="Female"/><form:errors cssClass="error" path="gender"/><br/><br> --%>
<tr><th><form:label path="gender">Gender</form:label></th>
<td><form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female </td></tr>
<%-- Address:
<form:input path="address" size="30"/><form:errors cssClass="error" path="address"/><br/><br> --%>
<tr><th><form:label path="address">Address</form:label></th>
<td><form:input path="address"/></td></tr>
<%-- Mobile No:
<form:input path="mobileno" size="30"/><form:errors cssClass="error" path="mobileno"/><br/><br> --%>
<tr><th><form:label path="mobileno">MobileNo</form:label></th>
<td><form:input path="mobileno"/></td></tr>
<tr><th></th><td>
<input type="submit" value="Add" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
<h3><a href="searchPatientById">searchPatientById</a></h3>
</form:form>
</div>
</body>
</html>