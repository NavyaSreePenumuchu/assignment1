<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Tracker</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
<style type="text/css">
.view{​​​​​​​
border-collapse:collapse;
background-color:cyan;
}
.bg-img{
background-image:url("E:/Pics/Prescp.jpg");
}​​​​​​​
</style>
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Add Prescription</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>


 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
     <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<div class="bg-img">
<table border="1" width="500" height="200">
<form:form action="savePrescription" modelAttribute="prescription" method="post">
<%-- Name:<form:input path="name"/><form:errors cssClass="error" path="name"/><br/><br> --%>
<tr><th><form:label path="name">Name</form:label></th>
<td><form:input path="name"/></td></tr>
<%-- Age:<form:input path="age"/><br/><br> --%>
<tr><th><form:label path="age">Age</form:label></th>
<td><input type="text" name="age" /></td></tr>
<%-- Weight:<form:input path="weight"/><br/><br> --%>
<tr><th><form:label path="weight">Weight</form:label></th>
<td><input type="text" name="weight" /></td></tr>
<%-- Gender:
Male:<form:radiobutton path="gender" value="Male"/>
Female:<form:radiobutton path="gender" value="Female"/><form:errors cssClass="error" path="gender"/><br/><br> --%>
<tr><th><form:label path="gender">Gender</form:label></th>
<td><form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female </td></tr>
<%-- MedicineName<form:input path="medicinename"/><form:errors cssClass="error" path="medicinename"/><br/><br> --%>
<tr><th><form:label path="medicinename">MedicineName</form:label></th>
<td><form:input path="medicinename"/></td></tr>
<%-- Take<form:input path="take"/><form:errors cssClass="error" path="take"/><br/><br> --%>
<tr><th><form:label path="take">Take</form:label></th>
<td><input type="text" name="take" /></td></tr>
<%-- Drug form<form:input path="drugform"/><form:errors cssClass="error" path="drugform"/><br/><br> --%>
<tr><th><form:label path="drugform">DrugForm</form:label></th>
<td><form:input path="drugform"/></td></tr>
<%-- Route<form:input path="route"/><form:errors cssClass="error" path="route"/><br/><br> --%>
<tr><th><form:label path="route">Route</form:label></th>
<td><form:input path="route"/></td></tr>
<%-- Duration<form:input path="duration"/><form:errors cssClass="error" path="duration"/><br/><br> --%>
<tr><th><form:label path="duration">Duration</form:label></th>
<td><input type="text" name="duration" /></td></tr>
<tr><th></th><td>
<input type="submit" value="Add" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
<h3><a href="searchPrescriptionById">searchPrescriptionById</a></h3>
</form:form>
</div>
</body>
</html>